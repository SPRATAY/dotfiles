import os
import subprocess

from libqtile import bar, widget, qtile
from libqtile.config import Screen

#from .colors import colors


screens = [
    Screen(
        wallpaper="~/.config/qtile/wallpaper/madara.jpg",
        wallpaper_mode="fill",
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Sep(),
                widget.CPU(format=' {freq_current}GHz {load_percent}%'),
                widget.Sep(),
                widget.Memory(measure_mem='G'),
                widget.Sep(),
                #widget.PulseVolume(), #(emoji="false", emoji_list=['󰸈', '󰕿', '󰖀', '󰕾']),
                widget.GenPollText(update_interval=1, func=lambda: "{}%".format(subprocess.check_output(os.path.expanduser("~/.config/qtile/settings/modules/pipewire")).decode("utf-8"))),
                widget.Sep(),
                widget.Battery(),
                widget.Sep(),
                #widget.UPowerWidget(),
                widget.Backlight(format='light: {percent:2.0%}', backlight_name='amdgpu_bl1'),
                #widget.WiFiIcon(),
                widget.Sep(),
                widget.KeyboardLayout(configured_keyboards=['us', 'ru'], update_interval=1),
                widget.Sep(),
                #widget.TextBox("default config", name="default"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                widget.Systray(),
                widget.Sep(),
                #widget.WidgetBox(text_open='', widgets=[
                #    widget.TextBox(text="This widget is in the box"),
                #    widget.Memory()
                #    ]
                #),
                widget.Clock(format="%d/%m/%Y | %H:%M[%a]"),
                widget.QuickExit(),
            ],
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

