from libqtile.config import Match
from libqtile import layout
from .colors import colors

# Layouts and layout rules


layout_conf = {
    'border_focus': colors['current_line'],
    'border_width': 2,
    'margin': 10
}


layouts = [
    layout.MonadTall(**layout_conf),
    layout.MonadWide(**layout_conf),
    layout.Bsp(**layout_conf),
    #layout.Matrix(columns=2, **layout_conf),
    layout.RatioTile(**layout_conf),
    #layout.Columns(),
    #layout.Tile(),
    layout.TreeTab(),
    layout.Max(),
    #layout.VerticalTile(),
    #layout.Zoomy(),
]


floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        #Match(wm_class="keepassxc"), 
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        #Match(title="keepassxc"),
    ]
)
