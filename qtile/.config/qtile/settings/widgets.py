from .colors import colors

widget_defaults = dict(
    font="JetBrainsMono NF",
    fontsize = 12,
    padding = 2,
)
extension_defaults = widget_defaults.copy()
