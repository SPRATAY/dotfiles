from libqtile.config import Key
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal


mod = "mod4"
alt = "mod1"
terminal = guess_terminal()

keys = [Key(key[0], key[1], *key[2:]) for key in [




    # Switch between windows in current stack pane
    ([mod], "j", lazy.layout.down()),
    ([mod], "k", lazy.layout.up()),
    ([mod], "h", lazy.layout.left()),
    ([mod], "l", lazy.layout.right()),
    #Открыть терминал
    ([mod], "Return", lazy.spawn(terminal)),

    # Переключение языка
    ([alt], "Shift_L", lazy.widget["keyboardlayout"].next_keyboard()),
    
    # Change window sizes (MonadTall)
    ([mod, "shift"], "l", lazy.layout.grow()),
    ([mod, "shift"], "h", lazy.layout.shrink()),
    
    # Переключения окон
    ([alt], "Tab", lazy.group.next_window()),


    # Toggle floating
    #([mod, "shift"], "f", lazy.window.toggle_floating()),
    
    #Сделать фул скрин
    ([alt], "Return", lazy.window.toggle_fullscreen()),

    # Move windows up or down in current stack
    ([mod, "shift"], "j", lazy.layout.shuffle_down()),
    ([mod, "shift"], "k", lazy.layout.shuffle_up()),

    # Toggle between different layouts as defined below
    ([mod], "Tab", lazy.next_layout()),
    ([mod, "shift"], "Tab", lazy.prev_layout()),

    # Kill window
    ([mod], "w", lazy.window.kill()),

    # Switch focus of monitors
    ([mod], "p", lazy.next_screen()),
    ([mod], "o", lazy.prev_screen()),

    # Restart Qtile
    ([mod, "control"], "r", lazy.restart()),

    ([mod, "control"], "q", lazy.shutdown()),
    ([mod], "r", lazy.spawncmd()),

    # Volume
    ([], "XF86AudioLowerVolume", lazy.spawn(
        "wpctl set-volume @DEFAULT_SINK@ 5%-"
    )),
    ([], "XF86AudioRaiseVolume", lazy.spawn(
        "wpctl set-volume @DEFAULT_SINK@ 5%+"
    )),
    ([], "XF86AudioMute", lazy.spawn(
        "wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
    )),
    ([], "XF86AudioMicMute", lazy.spawn(
        "wpctl set-mute @DEFAULT_SOURCE@ toggle"
    )),

    # Brightness
    ([], "XF86MonBrightnessUp", lazy.spawn("light -A 5")),
    ([], "XF86MonBrightnessDown", lazy.spawn("light -U 5")),
    
]]
