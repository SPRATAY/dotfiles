import os
import subprocess
import glob

from libqtile import qtile
from libqtile import hook

@hook.subscribe.startup_once
def autostart(autostart_folder):
    autostart_path = os.path.expanduser(f'~/.config/qtile/autostart/{autostart_folder}')
    autostart_files = glob.glob(os.path.join(autostart_path, '*.sh'))

    for file_path in autostart_files:
        subprocess.Popen([file_path])

autostart('all')
if qtile.core.name == "x11":
    autostart('xorg')
elif qtile.core.name == "wayland":
    autostart('wayland')
